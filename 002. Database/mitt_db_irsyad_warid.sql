CREATE DATABASE db_mitt

CREATE TABLE db_mitt.dbo.users (
	username nvarchar(50) COLLATE Latin1_General_CI_AS NOT NULL,
	password nvarchar(50) COLLATE Latin1_General_CI_AS NOT NULL,
	CONSTRAINT user_PK PRIMARY KEY (username)
);

CREATE TABLE db_mitt.dbo.user_skills (
	userSkillID nvarchar(50) COLLATE Latin1_General_CI_AS NOT NULL,
	username nvarchar(50) COLLATE Latin1_General_CI_AS NULL,
	skillID int NULL,
	skillLevelID int NULL,
	CONSTRAINT userSkillId_PK PRIMARY KEY (userSkillID)
);

CREATE TABLE db_mitt.dbo.skill_levels (
	skillLevelID int IDENTITY(1,1) NOT NULL,
	skillLevelName varchar(500) COLLATE Latin1_General_CI_AS NULL,
	CONSTRAINT skill_level_PK PRIMARY KEY (skillLevelID)
);

CREATE TABLE db_mitt.dbo.skills (
	skillID int IDENTITY(1,1) NOT NULL,
	skillName varchar(500) COLLATE Latin1_General_CI_AS NULL,
	CONSTRAINT skills_PK PRIMARY KEY (skillID)
);

CREATE TABLE db_mitt.dbo.user_profiles (
	id_userprofile_pk int IDENTITY(0,1) NOT NULL,
	name nvarchar(50) COLLATE Latin1_General_CI_AS NULL,
	address nvarchar(500) COLLATE Latin1_General_CI_AS NULL,
	bod date NULL,
	email nvarchar(50) COLLATE Latin1_General_CI_AS NULL,
	CONSTRAINT user_profiles_PK PRIMARY KEY (id_userprofile_pk)
);