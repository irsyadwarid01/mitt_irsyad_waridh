﻿using MITT.Commons.BaseModels;
using MITT.Commons.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MITT.Repository.Interfaces
{
    public interface ISkillModelAccess
    {
        Task<ResponseDto<List<SkillViewModel>>> GetAll();
        Task<ResponseDto<SkillViewModel>> GetById(long id);
        Task<ResponseDto<SkillViewModel>> PostCreate(SkillViewModel model);
        Task<ResponseDto<SkillViewModel>> PostUpdated(SkillViewModel model);
        Task<ResponseDto<SkillViewModel>> PostDelete(SkillViewModel model);
        Task<ResponseDto<SkillViewModel>> Search(SkillViewModel model);
    }
}
