﻿using Microsoft.Extensions.DependencyInjection;
using MITT.Repository.Implementations;
using MITT.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MITT.Repository.DependencyInjections
{
    public static class RepositoryDependencyInjections
    {
        public static IServiceCollection AddRepositoryServices(this IServiceCollection services)
        {
            services.AddTransient<ISkillModelAccess, SkillModelAccess>();
            return services;
        }
    }
}
