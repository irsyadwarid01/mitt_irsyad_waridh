﻿using MITT.Commons.BaseModels;
using MITT.Commons.ViewModels;
using MITT.Data.Context;
using MITT.Data.Entities;
using MITT.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace MITT.Repository.Implementations
{
    public class SkillModelAccess : ISkillModelAccess
    {
        private readonly MITTContext _context;

        public SkillModelAccess(MITTContext context)
        {
            _context = context;
        }

        public async Task<ResponseDto<SkillViewModel>> PostCreate(SkillViewModel model)
        {
            try
            {
                Skill data = new Skill
                {
                    SkillId = model.SkillId,
                    SkillName = model.SkillName,
                };
                _context.Skills.Add(data);
                await _context.SaveChangesAsync();
                return new ResponseDto<SkillViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }
        }
        public async Task<ResponseDto<SkillViewModel>> PostDelete(SkillViewModel model)
        {
            try
            {
                Skill a = new Skill
                {
                    SkillId = model.SkillId,
                    SkillName = model.SkillName,
                };
                _context.Update(a);
                await _context.SaveChangesAsync();
                return new ResponseDto<SkillViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }

        }

        public async Task<ResponseDto<List<SkillViewModel>>> GetAll()
        {
            try
            {
                List<SkillViewModel> data = await (from a in _context.Skills
                                                        select new SkillViewModel
                                                        {
                                                            SkillId = a.SkillId,
                                                            SkillName = a.SkillName
                                                        }).ToListAsync();

                return new ResponseDto<List<SkillViewModel>>(data);
            }
            catch (Exception ex)
            {
                return new ResponseDto<List<SkillViewModel>>(false, ex.Message);
            }
        }

        public async Task<ResponseDto<SkillViewModel>> GetById(long id)
        {
            try
            {
                SkillViewModel data = await (from a in _context.Skills
                                                  where a.SkillId == id
                                                  select new SkillViewModel
                                                  {
                                                      SkillId = a.SkillId,
                                                      SkillName = a.SkillName,
                                                  }).FirstOrDefaultAsync();
                return new ResponseDto<SkillViewModel>(data);
            }
            catch (Exception ex)
            {
                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }
        }

        public async Task<ResponseDto<SkillViewModel>> PostUpdated(SkillViewModel model)
        {
            try
            {
                Skill a = new Skill
                {
                    SkillId = model.SkillId,
                    SkillName = model.SkillName,
                };
                _context.Update(a);
                await _context.SaveChangesAsync();
                return new ResponseDto<SkillViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }
        }

        public Task<ResponseDto<SkillViewModel>> Search(SkillViewModel model)
        {
            throw new NotImplementedException();
        }

    }
}
