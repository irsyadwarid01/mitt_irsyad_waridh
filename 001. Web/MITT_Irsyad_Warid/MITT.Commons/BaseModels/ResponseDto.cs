﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MITT.Commons.BaseModels
{
    public class ResponseDto<T>
    {


        public ResponseDto()
        {
            IsSuccess = true;
            Message = string.Empty;
        }
        public ResponseDto(bool isSuccess, string message)
        {
            IsSuccess = isSuccess;
            Message = message;
        }

        public ResponseDto(bool isSuccess, string message, T data)
        {
            IsSuccess = isSuccess;
            Message = message;
            Data = data;
        }

        public ResponseDto(T data)
        {
            Data = data;
            IsSuccess = true;
            Message = string.Empty;
        }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
}
