﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MITT.Commons.ViewModels
{
    public class UserViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
