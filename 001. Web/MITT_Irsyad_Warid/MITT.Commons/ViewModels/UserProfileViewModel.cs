﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MITT.Commons.ViewModels
{
    public class UserProfileViewModel
    {
        public int IdUserprofilePk { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime? Bod { get; set; }
        public string Email { get; set; }
    }
}
