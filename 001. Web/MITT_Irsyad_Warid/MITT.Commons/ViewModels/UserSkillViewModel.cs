﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MITT.Commons.ViewModels
{
    public class UserSkillViewModel
    {
        public string UserSkillId { get; set; }
        public string Username { get; set; }
        public int? SkillId { get; set; }
        public int? SkillLevelId { get; set; }
    }
}
