﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MITT.Commons.ViewModels
{
    public class SkillLevelViewModel
    {
        public int SkillLevelId { get; set; }
        public string SkillLevelName { get; set; }
    }
}
