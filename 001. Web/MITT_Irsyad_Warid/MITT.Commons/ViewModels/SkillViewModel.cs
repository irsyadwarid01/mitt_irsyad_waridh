﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MITT.Commons.ViewModels
{
    public class SkillViewModel
    {
        public int SkillId { get; set; }
        public string SkillName { get; set; }
    }
}
