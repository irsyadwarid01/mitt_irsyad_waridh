﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MITT.Data.Entities
{
    public partial class Skill
    {
        public int SkillId { get; set; }
        public string SkillName { get; set; }
    }
}
