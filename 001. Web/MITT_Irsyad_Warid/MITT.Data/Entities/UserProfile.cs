﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MITT.Data.Entities
{
    public partial class UserProfile
    {
        public int IdUserprofilePk { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime? Bod { get; set; }
        public string Email { get; set; }
    }
}
