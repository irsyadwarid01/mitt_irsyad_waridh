﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MITT.Data.Entities
{
    public partial class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
