﻿using Microsoft.AspNetCore.Mvc;
using MITT.Commons.ViewModels;
using MITT.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MITT._01.Web.Controllers
{
    public class SkillController : Controller
    {
        private readonly ISkillModelAccess _skillModelAccess;

        public SkillController(ISkillModelAccess skillModelAccess)
        {
            _skillModelAccess = skillModelAccess;
        }
        public async Task<IActionResult> Index(string search)
        {
            var data = await _skillModelAccess.GetAll();
            List<SkillViewModel> data1 = new List<SkillViewModel>();

            if (data.IsSuccess)
            {
                if (search != null)
                {
                    foreach (var item in data.Data)
                    {
                        if (item.SkillName.ToLower().Contains(search.ToLower()))
                        {
                            SkillViewModel b = new SkillViewModel
                            {
                                SkillId = item.SkillId,
                                SkillName = item.SkillName,
                            };
                            data1.Add(b);
                        }
                    }
                }
                else
                {
                    foreach (var i in data.Data)
                    {
                        data1.Add(i);
                    }
                }
                return View(data1);
            }
            return View(data);
        }
        public IActionResult CreateMITT()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateMITT(SkillViewModel a)
        {
            await _skillModelAccess.PostCreate(a);

            return RedirectToAction("Index");
        }
        public async Task<IActionResult> EditMITT(int id)
        {
            var data = await _skillModelAccess.GetById(id);

            SkillViewModel a = new SkillViewModel();
            if (data.IsSuccess && data.Data != null)
            {
                a.SkillId = id;
                a.SkillName = data.Data.SkillName;
            };
            return View(a);
        }

        [HttpPost]
        public async Task<IActionResult> EditMITT(SkillViewModel a, int id)
        {
            await _skillModelAccess.PostUpdated(a);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteMITT(int id)
        {
            var data = await _skillModelAccess.GetById(id);

            SkillViewModel a = new SkillViewModel();
            if (data.IsSuccess && data.Data != null)
            {
                a.SkillId = id;
                a.SkillName = data.Data.SkillName;
            };
            return View(a);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteMITT(SkillViewModel a, int id)
        {
            var data2 = await _skillModelAccess.GetById(id);

            a.SkillName = data2.Data.SkillName;

            await _skillModelAccess.PostDelete(a);
            return RedirectToAction("Index");
        }
    }
}
